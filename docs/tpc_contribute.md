# OpenHarmony-TPC 开源三方库贡献

## 如何将开源三方库贡献到OpenHarmony-TPC

开发者贡献一个三方库到[OpenHarmony-TPC](https://gitee.com/openharmony-tpc)的流程是：三方库立项——>OpenHarmony-sig 孵化——>毕业准出到OpenHarmony-TPC

在该过程中我们需要先在[OpenHarmony-sig](https://gitee.com/openharmony-sig)上申请一个孵化仓，sig孵化过程是一个检验并保证三方库质量的过程。

### OpenHarmony-sig建仓

- 建仓申请

  在OpenHarmony 架构SIG会议上申报议题，议题内容是申请建三方库仓。上会时需要准备建仓申请材料，会议上需要说明 三方库的背景，能力，价值以及选型。建仓会议申请请参照文档[sig管理](https://gitee.com/openharmony/community/blob/master/zh/sig_governance.md#21--openharmony-%E9%A1%B9%E7%9B%AE%E4%BB%93%E5%BA%93%E7%AE%A1%E7%90%86%E6%B5%81%E7%A8%8B)

- 创建三方库SIG仓
  
  1. [登录OpenHarmony数字化平台](http://ci.openharmony.cn/workbench/cicd/codecontrol/list) <br>
    &nbsp;![登录](./media/ci_login1.png)
    &nbsp;![登录](./media/ci_login2.png)
  2. 选SIG管理下的SIG仓申请     <br>
    建仓具体步骤如下图所示：    <br>
    &nbsp;![建仓步骤](./media/create_step.png)
  3. SIG建仓内容填写    <br>
    &nbsp;![建仓内容填写](./media/infomations.png)  <br>
    ***建仓申请通过了构架SIG评审后，会议纪要会上传到:https://lists.openatom.io/hyperkitty/list/dev@openharmony.io/,通过该网址按时间搜索到具体的纪要信息***
  4. 填写完后联系 董金光[@dongjinguang](https://gitee.com/dongjinguang)进行审批，审批通过后仓库自动建成。

- 门禁配置

  门禁是保证代码质量是否达到质量要求关键手段之一，其主要包含代码编译、静态/安全/开源检查、敏感词/copyright扫描等。  <br>
  门禁申请的主要步骤如下：  <br>
  1. 在OpenHarmony-sig的[manifest仓]()创建对应三方仓的xml配置文件。
  2. 登录[OpenHarmony数字化平台](http://ci.openharmony.cn/workbench/cicd/codecontrol/list)，在SIG管理下选择流水线申请,具体如下图所示:   <br>
    &nbsp;![门禁内容](./media/process_info1.png)
    &nbsp;![门禁内容](./media/process_info2.png)

    门禁所需填写的内容如上图所示，其余部分使用默认内容即可。OpenHarmony-TPC开源三方库当前预编译命令:

    ```shell
    rm -rf tpc_resource && git clone https://gitee.com/openharmony-tpc/tpc_resource.git && chmod 777 tpc_resource/CITools/js/* && ./tpc_resource/CITools/js/prebuild.sh ${PR_URL}
    ```

    编译命令:

    ```shell
    ./tpc_resource/CITools/js/build.sh ${PR_URL}
    ```

  3. 流水线审批
    流水线提交申请后可以通过`当前处理人`一栏查看审批人员，可以联系审批人员对申请进行处理。

  流水线审批通过后，仓库的门禁就可以正常运行。

### OpenHarmony-sig孵化毕业

- 孵化毕业的流程
  1. 提交孵化申请
  2. 孵化预审
  3. 准出评审
  4. 孵化准出申请

孵化准出材料准备参考:[仓库孵化准出](https://gitee.com/openharmony/community/blob/master/zh/sig_governance.md#23-%E4%BB%93%E5%BA%93%E5%AD%B5%E5%8C%96%E5%87%86%E5%87%BA).
孵化预审与准出评审材料参照：[模板](https://gitee.com/openharmony/community/blob/master/sig/sig_architecture/meetings/repository_review_template.pptx).

### 合入OpenHarmony-TPC

三方仓孵化准出成功后自动合入到OpenHarmony-TPC。

## 参考资料
