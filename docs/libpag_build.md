# libpag 使用OpenHarmony SDK交叉编译指导说明

## 简介

libpag是PAG文件的实时渲染库，可在大多数平台（如iOS、Android、macOS、Windows、Linux和Web）上渲染基于矢量和光栅的动画。

## 编译前准备

### 编译环境

- ubuntu 20.04.1
- libpag版本（v4.3.51）
- HarmonyOS SDK版本(4.1.7.6及以上版本)
- nodejs版本(14.0及以上版本)

### 源码准备

- 获取代码
  
  ```shell
  # git clone https://github.com/Tencent/libpag.git -b v4.3.51
  ```

- 安装依赖

  下载完源码后需要下载对应的依赖包，执行源码中的`sync_deps.sh`即可(**执行同步依赖前需确保本地已安装了nodejs且版本必须是14.0或更新版本**)

  ```shell
  # ./sync_deps.sh
  ```

- HarmonyOS SDK获取

  HarmonyOS SDK获取需从[HarmonyOS developer 套件货架](https://developer.huawei.com/consumer/cn/deveco-developer-suite/cooperation/intention)上获取。

### 源码修改

libpag当前支持的平台是不包含OpenHarmony的，编译时需选择linux平台进行编译，并做一些修改，即可让libpag通过OpenHarmony的SDK进行交叉编译。

**1. linux平台添加arm64架构**

在`//third_party/vendor_tools/lib/Platform.js`中的平台配置中添加`arm64`

```shell
diff -Nur libpag/third_party/vendor_tools/lib/Platform.js libpag_ohos/third_party/vendor_tools/lib/Platform.js
--- libpag/third_party/vendor_tools/lib/Platform.js	2024-04-30 17:00:46.000000000 +0800
+++ libpag_ohos/third_party/vendor_tools/lib/Platform.js	2024-05-04 11:07:05.636183182 +0800
@@ -36,7 +36,7 @@
         if (name === "web") {
             return new WebPlatform(debug, verbose);
         }
-        return new Platform(name, ["x64"], debug, verbose);
+        return new Platform(name, ["x64","arm64"], debug, verbose);
     }
 
     constructor(name, archs, debug, verbose) {
```

**2. 设置交叉编译配置文件`CMAKE_TOOLCHAIN_FILE`**

- 在`//third_party/vendor_tools/lib/CMake.js`文件中的`LinuxCMake`配置中添加：

```shell
class LinuxCMake extends CMake {
  getPlatformArgs(arch) {    
      return [
                "-DCMAKE_POSITION_INDEPENDENT_CODE=ON",
                "-DCMAKE_TOOLCHAIN_FILE=/home/owner/sdk-linux/sdk/HarmonyOS-NEXT-DP2/base/native/build/cmake/ohos.toolchain.cmake"
            ];
  }
}
```

- `//third_party/tgfx/vendor.json`中所有的三方库编译都需要加上该配置:

```shell
...
vendors": [
    {
      "name": "skcms",
      "cmake": {
        "targets": [
          "skcms"
        ],
        "arguments": [
          "-DCMAKE_CXX_FLAGS=\"-w\"",
          "-DCMAKE_TOOLCHAIN_FILE=/home/owner/sdk-linux/sdk/HarmonyOS-NEXT-DP2/base/native/build/cmake/ohos.toolchain.cmake"
        ]
      }
    },
...
```

**特别声明：此处`/home/owner/sdk-linux/sdk/HarmonyOS-NEXT-DP2/base/`路径需配置为用户自己SDK的路径**

**3. 修改打包文件的后缀**

原生库通过linux分支是gcc编译出的.o文件进行打包成静态库，OpenHarmony的SDK通过clang编译，生产的产物是.obj文件，需在`//third_party/tgfx/third_party/vendor_tools/lib/LibraryTool.js`中进行修改：

```shell
diff -Nur libpag/third_party/tgfx/third_party/vendor_tools/lib/LibraryTool.js libpag_ohos/third_party/tgfx/third_party/vendor_tools/lib/LibraryTool.js
--- libpag/third_party/tgfx/third_party/vendor_tools/lib/LibraryTool.js	2024-04-30 17:01:48.000000000 +0800
+++ libpag_ohos/third_party/tgfx/third_party/vendor_tools/lib/LibraryTool.js	2024-05-04 11:10:51.460989770 +0800
@@ -170,7 +170,7 @@
                 this.extractStaticLibrary(ar, tempDir, library, verbose);
             }
             Utils.deletePath(output);
-            let cmd = ar + " rc " + Utils.escapeSpace(output) + " */*.o";
+            let cmd = ar + " rc " + Utils.escapeSpace(output) + " */*.obj";
             Utils.exec(cmd, tempDir, verbose);
             Utils.deletePath(tempDir);
             return;
```

**4. 修改动态库连接静态库的方式**

动态库链接静态库时，对一些未使用到的函数不会进行链接，为了避免使用中出现找不到对应函数的情况，需要在链接静态库时强制链接所有符号，在`//CMakeLists.txt`文件中修改动态库链接方式：

```shell
 diff -Nur libpag/CMakeLists.txt libpag_ohos/CMakeLists.txt
 --- libpag/CMakeLists.txt	2024-04-29 18:55:13.000000000 +0800
 +++ libpag_ohos/CMakeLists.txt	2024-05-04 15:03:01.821684728 +0800
 @@ -475,7 +475,7 @@
 target_compile_options(pag PUBLIC ${PAG_COMPILE_OPTIONS})
 target_link_options(pag PUBLIC ${PAG_LINK_OPTIONS})
 target_include_directories(pag PUBLIC include PRIVATE ${PAG_INCLUDES})
-target_link_libraries(pag ${PAG_STATIC_LIBS} ${PAG_SHARED_LIBS})
+target_link_libraries(pag  -Wl,--whole-archive ${PAG_STATIC_LIBS} -Wl,--no-whole-archive ${PAG_SHARED_LIBS})
 set_target_properties(pag PROPERTIES PREFIX "")
 set_target_properties(pag PROPERTIES OUTPUT_NAME libpag)
```

**5. 去掉编译警告当错误输出**

libpag配置了`-Werror`，会将警告当成错误导致编译失败，因此需对该配置进行修改，修改方法如下：

```shell
# cd libpag                                                         ## 进入到libpag根目录
# sed -i 's/-Werror /-Wno-error /g'  CMakeLists.txt                 ## 将根目录的CMakeLists.txt中配置的-Werror修改为-Wno-error
# cd third_party/tgfx                                               ## 进入到libpag依赖库tgfx目录
# sed -i 's/-Werror /-Wno-error /g'  CMakeLists.txt                 ## 将tgfx的CMakeLists.txt中配置的-Werror修改为-Wno-error
```

**6. 修改编译后所有符号为可见**

编译后有部分函数在动态库中的类型是`t`(内部定义，外部无法访问)导致编译时有部分接口无法使用，故需在编译时添加`-fvisibility=protected`使接口全部对外可见。

在`//CMakeLists.txt `的`PAG_COMPILE_OPTIONS`中添加：

```shell
--- libpag/CMakeLists.txt	2024-04-29 18:55:13.000000000 +0800
+++ libpag_ohos/CMakeLists.txt	2024-05-04 15:03:01.821684728 +0800
@@ -143,7 +143,7 @@
 list(APPEND PAG_FILES ${COMMON_FILES})
 
 if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
-    list(APPEND PAG_COMPILE_OPTIONS -Werror -Wall -Wextra -Weffc++ -pedantic -Werror=return-type)
+    list(APPEND PAG_COMPILE_OPTIONS -Wno-error -Wall -Wextra -Weffc++ -pedantic -Werror=return-type -fvisibility=protected)
 endif ()
```

在`//third_party/tgfx/CMakeLists.txt`的`PAG_COMPILE_OPTIONS`中添加：

```shell
--- libpag/third_party/tgfx/CMakeLists.txt	2024-04-30 17:00:49.000000000 +0800
+++ libpag_ohos/third_party/tgfx/CMakeLists.txt	2024-05-04 11:22:55.910446209 +0800
@@ -93,7 +93,7 @@
 list(APPEND TGFX_FILES ${PLATFORM_COMMON_FILES})
 
 if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
-    list(APPEND TGFX_COMPILE_OPTIONS -Werror -Wall -Wextra -Weffc++ -Wconversion -pedantic -Werror=return-type)
+    list(APPEND TGFX_COMPILE_OPTIONS -Wno-error -Wall -Wextra -Weffc++ -Wconversion -pedantic -Werror=return-type -fvisibility=protected)
 endif ()
```

**7. 修改`src/platform/Platform.h`文件**

`src/platform`是各平台适配层代码，当前适配层未添加`linux`的，库编译中有调用到`Platform::Current()`接口，因此将此接口定义为一个空函数：

```shell
diff -Nur libpag/src/platform/Platform.h libpag_ohos/src/platform/Platform.h
--- libpag/src/platform/Platform.h	2024-04-29 18:55:13.000000000 +0800
+++ libpag_ohos/src/platform/Platform.h	2024-05-04 11:35:57.738494232 +0800
@@ -40,7 +40,7 @@
   /**
    * Returns the instance of the current platform.
    */
-  static const Platform* Current();
+  static const Platform* Current() { return nullptr; };
 
   virtual ~Platform() = default;
```

至此，针对通过OpenHarmony SDk交叉编译需要做的修改已完成。[完整的patch文件](./libpag.patch)

## 编译

做完以上修改后，我们就可以进行编译了，在根目录执行以下编译命令：

```shell
# ./build_pag --platform linux --arch arm64                       # 当前只配置了编译arm64位的库文件
```

编译成功后库文件在`//out/release/linux/arm64/`目录下：

```shell
owner@owner-virtual-machine:~/workspace/thirdparty/libpag$ ls out/release/linux/arm64/
libpag.so
```

## IDE上使用

1. 将`libpag.so`拷贝到工程的`\\libpag\libs\arm64-v8a`目录下；
2. 将对应的头文件拷贝到工程的`\\libpag\src\main\cpp\thirdparty\pag\include`目录下(为了保证编译时头文件路径不会出错，建议参照原库路径拷贝头文件)；
3. 修改工程的`\\libpag\src\main\cpp\CMakeLists.txt`文件，

```shell
# 将三方库加入工程中
target_link_libraries(native_libpag PRIVATE ${NATIVERENDER_ROOT_PATH}/../../../libs/${OHOS_ARCH}/lib/libpag.so)
#将三方库的头文件加入工程中
target_include_directories(native_libpag PRIVATE ${NATIVERENDER_ROOT_PATH}/thirdparty/pag/include)
```

**特别说明:libpag依赖了libGLESv2.0，如若测试设备上没有该库，需要将该so从SDK中拷贝到工程的`\\libpag\libs\arm64-v8a`目录下一起打包到设备中。**


## 总结

通过当前方案可使用OpenHarmony的SDK编译出libpag，有几点需要注意：

1. `//src/platform/`中的`Current`接口直接返回的空指针，对于整体的影响未分析出。 <br>
   待改进做法：<br>
   需在`platform`下需实现`ohos`平台的适配层，不全`Current`接口的实现。

