# 应用如何调用C/C++三方库

## 简介

前面我们已经了解了[如何将一个C/C++三方库移植到OpenHarmony上](https://gitee.com/openharmony-sig/knowledge/blob/master/docs/openharmony_getstarted/port_thirdparty/README.md) ,那移植完的三方库我们的应用该如何调用呢？OpenHarmony上的应用一般都是js/ets语言编写的，而js/ets语言是无法直接调用C/C++接口的，所以我们需要在js/ets和C/C++之间建立一个可以互通的桥梁。OpenHarmony系统中提供的napi框架正是这么一座桥梁。<br>
OpenHarmony的napi介绍请参考[Hello Napi](https://gitee.com/javen678/hello-ohos-napi/blob/master/doc/1.HelloNAPI.md)

## 应用调用C/C++三方库的方式

- so形式调用    <br>
通过OpenHarmony的SDK编译，将三方库编译成so，和系统固件一起打包到系统rom中。
- hap形式调用   <br>
将三方库源码和应用源码放在一起，通过IDE编译最终打包到应用hap包中。

## 应用调用C/C++三方库实战

本文以minizip-ng三方库hap形式调用为例进行说明应用是如何调用C/C++三方库的。

### 移植适配

minizip-ng三方库的移植适配参考文档[通过IDE集成C/C++三方库](./adapter_thirdlib_with_ide.md).

### Napi接口开发

三方库napi的接口一般是由需求方提供的，对于无需求或需要自己定义接口的，我们可以根据三方库对外导出的API接口进行封装或是根据原生库的测试用例对外封装测试接口。本文中我们以封装2个minizip-ng测试接口为例详细说明napi接口开发的具体流程。<br>
napi接口开发前提是需要创建一个napi的工程，具体步骤参考[通过Deveco Studio创建一个Napi工程](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/napi_study/docs/hello_napi.md)

#### Napi接口定义

根据原生库的测试用例，我们封装2个测试用例接口:

```c++
int compress(void *data)     // 压缩功能，其中data是压缩时配置对应的属性数据
int decompress(void *data)   // 解压缩功能，其中data是解压缩时配置对应的属性数据
```

#### napi接口注册

```c++
napi_property_descriptor desc[] = {
    {"Compress", nullptr, Compress, nullptr, nullptr, nullptr, napi_default, nullptr},
    {"Decompress", nullptr, Decompress , nullptr, nullptr, nullptr, napi_default, nullptr}
};
```

#### napi接口实现

- 压缩功能接口实现

  ```c++
  napi_value Compress(napi_env env, napi_callback_info info)
  {
    napi_value result = nullptr;
    napi_get_undefined(env, &result);
    napi_value value;
    size_t argc = 1;
    
    if (napi_get_cb_info(env, info, &argc, &value, nullptr, nullptr) != napi_ok) {
        return result;
    }

    // 参数解析以及根据参数调用minizip的压缩接口
    if (minizip_parser_params(env, value) < 0) {
        return result;
    }
    
    if (napi_create_int64(env, 0, &result) != napi_ok) {
        std::cout << "napi_create_int64" << std::endl;
    }
    
    return result;
  }

  ```

- 解压缩功能接口实现
  
  ```c++
  napi_value Decompress(napi_env env, napi_callback_info info)
  {
    napi_value result = nullptr;
    napi_get_undefined(env, &result);
    napi_value value;
    size_t argc = 1;
    
    if (napi_get_cb_info(env, info, &argc, &value, nullptr, nullptr) != napi_ok) {
        return result;
    }

    // 参数解析以及根据参数调用minizip的解压缩接口
    if (minizip_parser_params(env, value) < 0) {
        return result;
    }
    
    if (napi_create_int64(env, 0, &result) != napi_ok) {
        std::cout << "napi_create_int64" << std::endl;
    }
    
    return result;
  }
  ```

- 参数解析接口

  ```c++
  int minizip_parser_params(napi_env env, napi_value object)
  {
    /*.
    ...
    参数定义
    ...
    */
    if (minizip_getObjectPropetry(env, object, "path", STRING, (void *)path) < 0) { // 获取解压缩文件的路径
        return -1;
    }

    minizip_getObjectPropetry(env, object, "password", STRING, (void *)password);   // 获取加压缩的密码，由此判断是否带密码的解压缩
    if (minizip_getObjectPropetry(env, object, "operate", STRING, (void *)optBuf) < 0) {    // 获取是压缩或解压缩功能
        return -1;
    }

    /*.
    ...
    其他参数解析
    ...
    */

    if (cmd == COMPRESS) {  // 压缩，直接调用minizip中封装的接口 minizip_add
        err = minizip_add((const char *)path, strlen(password) > 0 ? (const char *)password : nullptr, &opt, argc, (const char **)argv);
    } else if (cmd == DECOMPRESS) { // 解压缩，直接调用minizip中封装的接口 minizip_extract
        err = minizip_extract((const char *)path, strlen(file_extract) > 0 ? (const char *)file_extract : nullptr,
            strlen(destination) > 0 ? (const char *)destination : nullptr, strlen(password) > 0 ? (const char *)password : nullptr, &opt);
    }

    return err;
  }
  ```

### 应用调用napi接口

- 接口声明 <br>
  在确定需要封装的接口后，我们需要将这些接口定义在index.d.ts文件中(路径entry/src/main/cpp/types/libentry/index.d.ts)

  ```js
  export interface miniOptions {
    include_path?:number
    compress_level?:number
    compress_method?:number
    overwrite?:number
    append?:number
    disk_size?:number
    follow_links?:number
    store_links?:number
    zip_cd?:number
    encoding?:number
    verbose?:number
    aes?:number
    cert_path?:string
    cert_pwd?:string
  }

  export interface miniProperty {
    path:string
    password?:string
    operate?:string
    file_extract?:string
    option:miniOptions
    directory?:string
    files?:Array<string>
  }
  export const compress: (data:miniProperty) => number
  export const decompress: (data:miniProperty) =>number;
  ```

- 导入so文件    <br>
  根据napi注册时的模块名字导入对应的库

  ```ts
  import minizip from "@ohos.minizipNapi"
  ```

- JS应用调用接口    <br>
  导入库后我们就可以直接调用上面定义的2个接口了，本示例中我们创建了2个按钮，通过2个按钮分别实现压缩和解压缩的功能
  
  ```ts
  build() {
    Column() {
      Button(this.button0Txt)
        .fontSize(50)
        .margin({top:30})
        .fontWeight(FontWeight.Normal)
        .onClick(() => {
          try {
            let result = minizip.compress({ path: this.dir + "result.zip", operate: "compress",
                option: { append: 1, compress_level: 9 },
                files: [this.dir + "file1.txt"] })
            if (result == undefined) {
                this.button0 = 0;
                this.button0Txt = "compressEpty"
            }
            console.info("[Minizip]compress result = " + JSON.stringify(result));
          }catch(e) {
            console.info("[Minizip] error : " + JSON.stringify(e));
          }
        })
      Button(this.button1Txt)
        .fontSize(40)
        .margin({top:30})
        .fontWeight(FontWeight.Normal)
        .onClick(() => {
          try {
            let result = minizip.decompress({path:this.dir + "result.zip", operate:"decompress",
                option:{overwrite:1, compress_level:9}, directory:this.dir + "out"})
            if (result == undefined) {
                this.button0 = 0;
                this.button0Txt = "compressEpty"
            }
            console.info("[Minizip]compress result = " + JSON.stringify(result));
          }catch(e) {
            console.info("[Minizip] error : " + JSON.stringify(e));
          }
        })
  ```

具体使用参考文档[minizip-ng应用包集成使用说明](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/minizip-ng/docs/hap_integrate.md)

## 参考文档

- [通过IDE集成C/C++三方库](./adapter_thirdlib_with_ide.md)
- [minizip-ng应用包集成使用说明](https://gitee.com/openharmony-sig/tpc_c_cplusplus/blob/master/minizip-ng/docs/hap_integrate.md)
- [OpenHarmony知识体系](https://gitee.com/openharmony-sig/knowledge/tree/master)
