
# 三方库知识脉络

# 三方库生态

## 当前哪些三方库可用

- [OpenHarmony三方组件资源汇总](https://gitee.com/openharmony-tpc/tpc_resource)

## 当前三方库规划

- [OpenHarmony三方组件规划](xx)(暂缺)

## 问题反馈

- [三方库问题反馈指导](xxxx)(暂缺)

# 三方库的使用

## C/C++三方库的使用

### 北向应用中使用

- :tw-1f195: [通过Deveco Studio创建一个Napi工程](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/napi_study/docs/hello_napi.md)
- :tw-1f195: [Napi 数据类型与同步调用](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/napi_study/docs/napi_data_type.md)
- :tw-1f195: [Napi 异步调用](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/napi_study/docs/napi_asynchronous_call.md)
- :tw-1f195: [Napi 生命周期](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/napi_study/docs/napi_life_cycle.md)
- :tw-1f195: [Napi 对象导出](https://gitee.com/openharmony-sig/knowledge_demo_temp/blob/master/docs/napi_study/docs/napi_export_object.md)
- :tw-1f195: [OpenHarmony北向应用如何引用C/C++三方库](./docs/app_call_thirdlib_hap.md)

### 南向应用中使用

- [OpenHarmony南向应用如何引用C/C++三方库](xx)(暂缺)(1.13)

# 三方库适配

## C/C++ 三方库适配方法

### 在固件包（ROM）中集成三方库

- [什么是OH的固件包(ROM)](xx)(暂缺)(1.13)
- [OH固件包编译构建的分析](xx)(暂缺)(1.13)
- [如何将三方库集成到固件包中](xx)(暂缺)(1.13)
- [如何在固件包中快速集成三方库](xx)(暂缺)(1.13)
- [如何测试验证固件中集成的三方库](xx)(暂缺)(1.13)

### 在应用包（hap）中集成三方库

- :tw-1f195: [什么是OH的应用包(hap)](./docs/hap.md)
- :tw-1f195: [OH应用包编译构建分析](./docs/hap_build.md)
- :tw-1f195: [如何将三方库集成到hap包中](./docs/adapter_thirdlib_with_ide.md)
- :tw-1f195: [在OpenHarmony开发板上验证hap包中集成的三方库](./docs/test_hap.md)
- [在HamonyOS手机上验证hap包中集成的三方库](xx)(暂无)

### 编译三方库

- [Linux环境使用 OHOS SDK 编译三方库](./docs/ohos_use_sdk/OHOS_SDK-Usage.md)
- [OpenHarmony C/C++三方库移植适配指导说明](./docs/thirdparty_port.md)
- [WebRTC适配OpenHarmony应用端SDK指导](./docs/webrtc_adapter_ohos.md)
  
## C/C++ 三方库适配的关键问题点分析

### C/C++ 三方库适配风险点分析

- [什么是风险依赖](xxx)(暂缺)(1.20)
- [风险依赖有哪些](xxx)(暂缺)(1.20)
- [如何识别风险依赖](xxx)(暂缺)(1.20)
- [三方库的依赖库分析](xxx)(暂缺)(1.20)

### 固件集成的动态库无法在IDE上使用

- [应用集成和固件集成中C库差异化分析](./docs/rom_hap_c_cpluplus_diff.md)

## 典型三方库适配分享

### C/C++典型三方库适配分享

- [一文带你读懂如何移植三方库到OpenHarmony](https://mp.weixin.qq.com/s/4xIffuTuXCWfhLijwsklKA)
- [minizip-ng](xx)(暂缺)(1.20)
- [jbig2enc](xx)(暂缺)(1.20)

# 三方库质量分析

- [如何分析C/C++三方库内存泄漏的问题](xx)(暂缺)
- [如何降低C/C++三方库的功耗](xx)(暂缺)
- [如何提升C/C++三方库的性能](xx)(暂缺)

# 三方库典型样例

## C/C++三方库典型样例

- [本地车牌识别样例](https://gitee.com/openharmony-sig/knowledge_demo_travel/blob/master/docs/GreyWolf_EasyPR/readme.md)
- [人脸识别样例](https://gitee.com/openharmony-sig/knowledge_demo_travel/tree/master/docs/FaceRecognition_NAPI)

## JS/TS 三方库典型样例

- [Jchardet——支持检测并输出文件编码方式的组件](https://mp.weixin.qq.com/s/cB-ZLunMR1fMTkBLiyOq3Q)
- [Ohos-MPChart——支持多种图表绘制的组件](https://mp.weixin.qq.com/s/vqiYRwq_h4xalVjbw_W1Jg)
- [PhotoView——支持图片缩放、平移、旋转的一个优雅的三方组件](https://mp.weixin.qq.com/s/Zs8tnSU9ihvNSn01pHzsuA)
- [CircleIndicator组件，使指示器风格更加多样化](https://mp.weixin.qq.com/s/4rpW0AWtmcxq9GViceiwow)
- [ImageKnife组件，让小白也能轻松搞定图片开发](https://mp.weixin.qq.com/s/8KhgYoCtClgMxFJugwBcRg)

# 三方库贡献渠道和流程

- [如何贡献一个三方库](xx)(暂缺)
- [三方库上仓指导](xx)(暂缺)
- [C/C++三方库发布指导](xx)(暂缺)
- [JS/TS三方库发布指导](xx)(暂缺)

# FAQ
